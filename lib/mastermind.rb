class Code
  attr_reader :pegs
  PEGS = { "r" => "red",
           "g" => "green",
           "b" => "blue",
           "y" => "yellow",
           "o" => "orange",
           "p" => "purple"}

  def initialize(code_arr)
    @pegs = code_arr
  end

  def [](index)
    pegs[index]
  end

  def self.random
    code_arr = 4.times.map { PEGS.keys.sample }
    Code.new(code_arr)
  end

  def self.parse(code_str)
    code_str = code_str.downcase
    if valid_code?(code_str)
      Code.new(code_str.chars)
    else
      raise "Invalid colors entered"
    end
  end

  def self.valid_code?(code_str)
    code_str.each_char.all? { |char| PEGS.keys.include?(char) }
  end

  def ==(guessed_code)
    guessed_code.is_a?(Code) && pegs == guessed_code.pegs
  end

  def exact_matches(guessed_code)
    guessed_code.pegs.select.with_index do |color, i|
       pegs[i] == color
    end.length
  end

  def near_matches(guessed_code)
    test_pegs = pegs.dup
    matches = 0
    guessed_code.pegs.each do |color|
      if test_pegs.include?(color)
        matches += 1
        test_pegs.delete_at(test_pegs.index(color))
      end
    end
    matches - exact_matches(guessed_code)
  end
end


class Game
  attr_reader :secret_code, :get_guess, :guessed_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    print "Enter a four letter color code (RGBYOP): "
    input = $stdin.gets.chomp
    @guessed_code = Code.parse(input)
  end

  def display_matches(guessed_code)
    exact = secret_code.exact_matches(guessed_code)
    near = secret_code.near_matches(guessed_code)
    puts "exact: #{exact}, near: #{near}"
  end

  def play
    num_turns = 0
    while num_turns < 10
      display_matches(get_guess)
      num_turns += 1
      if secret_code == guessed_code
        puts "Congrats you won! You used #{num_turns} turns."
        break
      end
      puts "Turns: #{num_turns} / 10"
    end
    puts "Sorry, you're out of turns :/"
  end
end

if __FILE__ == $PROGRAM_NAME
  game = Game.new
  game.play
  p game.secret_code.pegs
  p game.guessed_code.pegs
end
